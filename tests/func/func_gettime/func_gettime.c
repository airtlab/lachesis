/******************************************************************************
 *
 *   Copyright © International Business Machines  Corp., 2007, 2008
 *
 *   This program is free software;  you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY;  without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 *   the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program;  if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * NAME
 *    tc-2.c
 *
 * DESCRIPTION
 *    Check if clock_gettime is working properly.
 *    This test creates NUMSLEEP threads that just sleep and NUMWORK threads
 *    that spend time on the CPU. It then reads the thread cpu clocks of all
 *    these threads and compares the sum of thread cpu clocks with the process
 *    cpu clock value. The test expects that:
 *    the cpu clock of every sleeping thread shows close to zero value.
 *    sum of cpu clocks of all threads is comparable with the process cpu clock.
 *
 *
 * USAGE:
 *    Use run_auto.sh script in current directory to build and run test.
 *
 * AUTHOR
 *    Sripathi Kodi <sripathik@in.ibm.com>
 *
 * HISTORY
 *    2007-Apr-04:  Initial version by Sripathi Kodi <sripathik@in.ibm.com>
 *
 *****************************************************************************/

#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <librttest.h>

#define THRESHOLD 0.5  /* 500 milliseconds */
#define NUMSLEEP 5
#define NUMWORK 2

RTIME sleepts[NUMSLEEP];
RTIME workts[NUMWORK];

void usage(void)
{
	rt_help();
	printf("thread_clock specific options:\n");
}

int parse_args(int c, char *v)
{
	int handled = 1;
	switch (c) {
	case 'h':
		usage();
		exit(0);
	default:
		handled = 0;
		break;
	}
	return handled;
}

void *workerthread(void *arg)
{
	int tid = (int)arg;

#ifdef DEBUG
	printf("Worker thread %d working\n", tid);
#endif

	RTIME startw = rt_gettime();
	rt_busywork(5*NS_PER_MS);
	RTIME stopw = rt_gettime();
	workts[tid] = stopw - startw;

#ifdef DEBUG
	printf("workerthread %d: after work CPU time (nsec): %ld\n", tid, workts[tid]);
#endif
	return NULL;
}

void *sleeperthread(void *arg)
{
	int tid = (int)arg;

#ifdef DEBUG
	printf("Sleeper thread %d sleeping\n", tid);
#endif

	RTIME starts = rt_gettime();
	rt_nanosleep(2*NS_PER_SEC);
	RTIME stops = rt_gettime();
	sleepts[tid] = stops - starts - 2*NS_PER_SEC;
	
#ifdef DEBUG
	printf("sleeperthread %d: after sleep CPU time (nsec): %ld\n", tid, sleepts[tid]);
#endif
	return NULL;
}

void checkresult(float proctime)
{
	int i;
	float diff, threadstime=0;
	for (i=0; i<NUMSLEEP; i++) {
		threadstime += sleepts[i];
	}
	for (i=0; i<NUMWORK; i++) {
		threadstime += workts[i];
	}
	diff = proctime - threadstime;
	if (diff < 0) diff = -diff;
	printf("Process: %.6f s\n", proctime / NS_PER_SEC);
	printf("Threads: %.6f s\n", threadstime / NS_PER_SEC + 2);
	printf("Delta:   %.6f s\n", diff / NS_PER_SEC - 2);
}

int main(int argc,char* argv[])
{
	int i;
	RTIME start, stop;
	task t = taskset_init(NUMSLEEP+NUMWORK);

	setup();

	pass_criteria = THRESHOLD;
	rt_init("ht:",parse_args,argc,argv);

	start = rt_gettime();
	/* Start sleeper threads */
	for (i=0; i<NUMSLEEP; i++, t++) {
		t->func = sleeperthread;
		t->arg = (void *)(intptr_t)i;
		if (task_create(t) < 0)
			exit(1);
	}
	printf("\n%d sleeper threads created\n", NUMSLEEP);

	/* Start worker threads */
	for (i=0; i<NUMWORK; i++, t++)
	{
		t->func = workerthread;
		t->arg = (void *)(intptr_t)i;
		if (task_create(t) < 0)
			exit(1);
	}
	printf("\n%d worker threads created\n", NUMWORK);

	printf("\nPlease wait...\n\n");

	task_join_all();
	/* Get the process cpu clock value */
	stop = rt_gettime();
	checkresult(stop-start);
	return 0;
}