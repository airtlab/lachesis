# Adding test subdirectories
add_subdirectory(blocktime_mutex)
add_subdirectory(jitter_sched)
add_subdirectory(latency_gtod)
add_subdirectory(latency_hrtimer)
# add_subdirectory(latency_kill)
add_subdirectory(latency_rdtsc)  
add_subdirectory(latency_sched)  
add_subdirectory(latency_signal)  
# Deleted for the moment ...
# add_subdirectory(matrix_mult)