/******************************************************************************
 *
 *	 This program is free software;	you can redistribute it and/or modify
 *	 it under the terms of the GNU General Public License as published by
 *	 the Free Software Foundation; either version 2 of the License, or
 *	 (at your option) any later version.
 *
 *	 This program is distributed in the hope that it will be useful,
 *	 but WITHOUT ANY WARRANTY;	without even the implied warranty of
 *	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See
 *	 the GNU General Public License for more details.
 *
 *	 You should have received a copy of the GNU General Public License
 *	 along with this program;	if not, write to the Free Software
 *	 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * NAME
 *	stress-pi.c
 *
 * DESCRIPTION
 *
 *
 * USAGE:
 *	Use run_auto.sh script in current directory to build and run test.
 *
 * AUTHOR
 *
 *
 * HISTORY
 *
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <librttest.h>

void usage(void)
{
	rt_help();
	printf("stress-pi specific options:\n");
}

int parse_args(int c, char *v)
{
	int handled = 1;
	switch (c) {
	case 'h':
		usage();
		exit(0);
	default:
		handled = 0;
		break;
	}
	return handled;
}

int gettid(void)
{
	return syscall(__NR_gettid);
}

#define THREAD_STOP		1

mutex glob_mutex;

//task arg1, arg2, arg3, arg4, arg5;

// int startThread(task thr);
// void stopThread(task thr);
// void joinThread(task thr);

void* func_nonrt(void* arg)
{
	task pthr = task_get((int)arg);
	int rc, i, j, policy, tid = gettid();
	struct sched_param schedp;
	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(0, &mask);

	rc = sched_setaffinity(0, sizeof(mask), &mask);
	if (rc < 0) {
		 printf("Thread %d: Can't set affinity: %d %s\n", tid, rc, strerror(rc));
		 exit(-1);
	}

// 	printf("Thread started %d on CPU %ld\n", pthr->prio, (long)mask.__bits[0]);
// 	pthread_getschedparam(pthr->pthread, &policy, &schedp);
// 	printf("Thread running %d\n", pthr->prio);

	while (1) {
		mutex_lock(&glob_mutex);
/*		printf("Thread %d at start pthread pol %d pri %d - Got global lock\n",
		    pthr->prio, policy, schedp.sched_priority);*/
		sleep(2);
		for (i = 0; i < 10000;i++) {
			if ((i % 100) == 0) {
				sched_getparam(tid, &schedp);
				policy = sched_getscheduler(tid);
				printf("Thread %d(%d) loop %d pthread pol %d "
				    "pri %d\n", tid, pthr->prio, i, policy,
				    schedp.sched_priority);
				fflush(NULL);
			}
			pthr->id++;
			for (j = 0;j < 5000; j++) {
				mutex_lock(&(pthr->lock));
				mutex_unlock(&(pthr->lock));
			}
		}
		mutex_unlock(&glob_mutex);
		pthread_yield();
	}
	return NULL;
}

void* func_rt(void* arg)
{
	task pthr = task_get((int)arg);
	int rc, i, j, policy, tid = gettid();
	struct sched_param schedp;
	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(0, &mask);

	rc = sched_setaffinity(0, sizeof(mask), &mask);
	if (rc < 0) {
		 printf("Thread %d: Can't set affinity: %d %s\n", tid, rc, strerror(rc));
		 exit(-1);
	}
	rc = sched_getaffinity(0, sizeof(mask), &mask);

	printf("Thread started %d on CPU %ld\n", pthr->prio, (long)mask.__bits[0]);
	//pthread_getschedparam(pthr->desc, &policy, &schedp);

	while (1) {
		sleep(2);
		printf("Thread running %d\n", pthr->prio);
		mutex_lock(&glob_mutex);
		printf("Thread %d at start pthread pol %d pri %d - Got global lock\n", pthr->prio,
			 policy, schedp.sched_priority);

		/* we just use the mutex as something to slow things down */
		/* say who we are and then do nothing for a while.	The aim
		 * of this is to show that high priority threads make more
		 * progress than lower priority threads..
		 */
		for (i=0;i<1000;i++) {
			if (i%100 == 0) {
				sched_getparam(tid, &schedp);
				policy = sched_getscheduler(tid);
				printf("Thread %d(%d) loop %d pthread pol %d pri %d\n", tid, pthr->prio, i,
			 policy, schedp.sched_priority);
				fflush(NULL);
			}
			pthr->id++;
			for (j = 0; j < 5000; j++) {
				mutex_lock(&(pthr->lock));
				mutex_unlock(&(pthr->lock));
			}
		}
		mutex_unlock(&glob_mutex);
		sleep(2);
	}
	return NULL;
}

void* func_noise(void* arg)
{
	task pthr = task_get((int)arg);
	int rc, i, j, policy, tid = gettid();
	struct sched_param schedp;
	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(0, &mask);

	rc = sched_setaffinity(0, sizeof(mask), &mask);
	if (rc < 0) {
		 printf("Thread %d: Can't set affinity: %d %s\n", tid, rc, strerror(rc));
		 exit(-1);
	}
	rc = sched_getaffinity(0, sizeof(mask), &mask);

	printf("Noise Thread started %d on CPU %ld\n", pthr->prio, (long)mask.__bits[0]);
	//pthread_getschedparam(pthr->desc, &policy, &schedp);

	while (1) {
		sleep(1);
		printf("Noise Thread running %d\n", pthr->prio);

		for (i = 0; i < 10000; i++) {
			if ((i % 100) == 0) {
				sched_getparam(tid, &schedp);
				policy = sched_getscheduler(tid);
				printf("Noise Thread %d(%d) loop %d pthread pol %d pri %d\n", tid, pthr->prio, i, policy, schedp.sched_priority);
				fflush(NULL);
			}
			pthr->id++;
			for (j = 0; j < 5000; j++) {
				mutex_lock(&(pthr->lock));
				mutex_unlock(&(pthr->lock));
			}
		}
		pthread_yield();
	}
	return NULL;
}

// int startThread(task thrd)
// {
// 	struct sched_param schedp;
// 	pthread_condattr_t condattr;
// 	int retc, policy, inherit;
// 
// 	printf("Start thread priority %d\n", thrd->prio);
// 	if (pthread_attr_init(&(thrd->attr)) != 0) {
// 		printf("Attr init failed");
// 		exit(2);
// 	}
// 	thrd->flags = 0;
// 	memset(&schedp, 0, sizeof(schedp));
// 	schedp.sched_priority = thrd->prio;
// 	policy = thrd->policy;
// 
// 	if (pthread_attr_setschedpolicy(&(thrd->attr), policy) != 0) {
// 		printf("Can't set policy %d\n", policy);
// 	}
// 	if (pthread_attr_getschedpolicy(&(thrd->attr), &policy) != 0) {
// 		printf("Can't get policy\n");
// 	} else {
// 		printf("Policy in attribs is %d\n", policy);
// 	}
// 	if (pthread_attr_setschedparam(&(thrd->attr), &schedp) != 0) {
// 		printf("Can't set params");
// 	}
// 	if (pthread_attr_getschedparam(&(thrd->attr), &schedp) != 0) {
// 		printf("Can't get params");
// 	} else {
// 		printf("Priority in attribs is %d\n", schedp.sched_priority);
// 	}
// 	if (pthread_attr_setinheritsched(&(thrd->attr), PTHREAD_EXPLICIT_SCHED) != 0) {
// 		printf("Can't set inheritsched\n");
// 	}
// 	if (pthread_attr_getinheritsched(&(thrd->attr), &inherit) != 0) {
// 		printf("Can't get inheritsched\n");
// 	} else {
// 		printf("inherit sched in attribs is %d\n", inherit);
// 	}
// 	if ((retc = mutex_init(&(thrd->lock))) != 0) {
// 		printf("Failed to init mutex: %d\n", retc);
// 	}
// 	if (pthread_condattr_init(&condattr) != 0) {
// 		printf("Failed to init condattr\n");
// 	}
// 	if (pthread_cond_init(&(thrd->cond), &condattr) != 0) {
// 		printf("Failed to init cond\n");
// 	}
// 	retc = pthread_create(thrd->desc,&(thrd->attr), thrd->func, thrd);
// 	printf("Create returns %d\n\n", retc);
// 	return retc;
// }

// void stopThread(task thr)
// {
// 	thr->flags += THREAD_STOP;
// 	joinThread(thr);
// }
// 
// void joinThread(task thr)
// {
// 	void* ret = NULL;
// 	if (pthread_join(thr->pthread, &ret) != 0) {
// 		printf("Join failed\n");
// 	}
// 	printf("Join gave %p\n", ret);
// }

/*
 * Test pthread creation at different thread priorities.
 */
int main(int argc, char* argv[]) {
	int i, retc, nopi = 0;
	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(0, &mask);
	setup();
	task t = taskset_init(5);

	rt_init("h", parse_args, argc, argv);

	retc = sched_setaffinity(0, sizeof(mask), &mask);
	if (retc < 0) {
		 printf("Main Thread: Can't set affinity: %d %s\n", retc, strerror(retc));
		 exit(1);
	}
	retc = sched_getaffinity(0, sizeof(mask), &mask);

	/*
	 * XXX: Have you ever heard of structures with c89/c99?
	 * Inline assignment is a beautiful thing.
	 */
	t[0].policy = SCHED_OTHER;
	t[0].prio = 0;
	t[0].func = func_nonrt;
	t[0].arg = 0;
	task_create(&t[0]);
	for (i=1; i<4; i++)
	{
		t[i].policy = SCHED_RR;
		t[i].prio = 20+i*10;
		t[i].func = func_rt;
		t[i].arg = (void*)i;
		task_create(&t[i]);
	}
	t[i].policy = SCHED_RR;
	t[i].prio = 40;
	t[i].func = func_noise;
	t[i].arg = (void*)i;
	task_create(&t[i]);

	for (i = 0;i < argc; i++) {
		if (strcmp(argv[i], "nopi") == 0)
			nopi = 1;
	}

	printf("Start %s\n",argv[0]);

	mutex_init ( &glob_mutex );
	
	sleep(10);

	task_join_all();
	printf("Done\n");

	return 0;
}
